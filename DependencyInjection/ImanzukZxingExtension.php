<?php

namespace Imanzuk\ZxingBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ImanzukZxingExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (!isset($config['enabled']) && !$config['enabled']) return;

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter(
            'zxing_bridge_lib_path', __DIR__.'/../Lib/zxing'
        );
        if (isset($config['temp_path']))
            $container->setParameter(
                'zxing_bridge_temp_path', $config['temp_path']
            );
        else
            $container->setParameter(
                'zxing_bridge_temp_path', $container->getParameter('imanzuk_zxing_temp_path')
            );
        if (isset($config['ghostscript_binary']))
            $container->setParameter(
                'zxing_bridge_gs_binary', $config['ghostscript_binary']
            );
        else
            $container->setParameter(
                'zxing_bridge_gs_binary', $container->getParameter('imanzuk_zxing_ghostscript_binary')
            );
        if (isset($config['use_nailgun']))
            $container->setParameter(
                'zxing_bridge_gs_binary', $config['use_nailgun']
            );
        else
            $container->setParameter(
                'zxing_bridge_use_nailgun', $container->getParameter('imanzuk_zxing_use_nailgun')
            );
    }
}
