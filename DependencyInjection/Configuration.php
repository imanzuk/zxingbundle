<?php

namespace Imanzuk\ZxingBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('imanzuk_zxing');

        $rootNode
            ->children()
                ->booleanNode('enabled')
                    ->defaultValue(true)
                ->end()
                ->scalarNode('ghostscript_binary')
                ->end()
                ->scalarNode('temp_path')
                ->end()
                ->booleanNode('use_nailgun')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
