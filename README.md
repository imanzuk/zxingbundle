ZxingBridge Bundle
==================
Allows easy use of ZXing barcode image processing library (https://code.google.com/p/zxing/).
Optionally zxing can be run via nailgun (http://www.martiansoftware.com/nailgun/).
