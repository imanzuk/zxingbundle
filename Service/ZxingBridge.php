<?php

namespace Imanzuk\ZxingBundle\Service;

use Symfony\Component\HttpKernel\Log\LoggerInterface;

class ZxingBridge {

    private $logger;
    private $gsPath;
    private $tempPath;
    private $zxingLibPath;
    private $useNailgun;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function getGsPath() {
        return $this->gsPath;
    }

    public function setGsPath($path) {
        $this->gsPath = $path;
    }

    public function getTempPath() {
        return $this->tempPath;
    }

    public function setTempPath($path) {
        $this->tempPath = $path;
        if (!file_exists($this->tempPath))
            mkdir($this->tempPath);
    }

    public function getZxingLibPath() {
        return $this->zxingLibPath;
    }

    public function setZxingLibPath($path) {
        $this->zxingLibPath = $path;
    }

    public function getUseNailgun() {
        return $this->useNailgun;
    }

    public function setUseNailgun($useNailgun) {
        $this->useNailgun = $useNailgun;
    }

    public function getZxingCmd() {
        $className = 'com.google.zxing.client.j2se.CommandLineRunner';
        if ($this->useNailgun)
            //TODO?: "Although there are means to ensure that the client is connected to the server from the local machine,
            //there is not yet any concept of a "user". Any programs that run in Nailgun are run with the same permissions
            //as the server itself."
            //http://www.martiansoftware.com/nailgun/
            return "ng $className";
        else
            return 'java -cp ' .
                $this->zxingLibPath.'/core-2.3.0.jar:' .
                $this->zxingLibPath.'/javase-2.3.0.jar ' .
                "$className"
            ;
    }

    public function decode($filePath) {
        $res = false;
        $cmdDecode = sprintf(
            '%s --possibleFormats=QR_CODE --dump_results --try_harder "%s"',
            $this->getZxingCmd(),
            $filePath
        );
        $this->logger->debug(sprintf(
            "trying to decode qr code: %s", $cmdDecode
        ));
        $filePathResult = str_replace('.png', '.txt', $filePath);
        if (file_exists($filePathResult)) unlink($filePathResult);
        $out = array();
        $res = null;
        exec($cmdDecode . ' 2>&1', $out, $res);
        $this->logger->debug(sprintf(
            "decoder exit code: %s, output: %s", $res, json_encode($out)
        ));
        if ($res != 0) {
            throw new \RuntimeException('qr code decoder failed');
        }
        if (!file_exists($filePathResult)) {
            $this->logger->debug("no qr code found");
            return false;
        }
        $res = file_get_contents($filePathResult);
        unlink($filePathResult);
        $this->logger->debug("qr code found: '$res'");
        return $res;
    }

    public function decodeFromPdf($filePath) {
        $filePathPng = str_replace('.pdf', '.png', $filePath);
        $filePathPng = $this->getTempPath() .
            substr($filePathPng, strrpos($filePathPng, DIRECTORY_SEPARATOR));
        $res = $this->convertPdfFirstPageToPng($filePath, $filePathPng);
        if ($res != 0) {
            throw new \RuntimeException('pdf to png conversion failed');
        }
        if (!file_exists($filePathPng)) {
            throw new \RuntimeException('png file not found');
        }
        $res = $this->decode($filePathPng);
        return $res;
    }

    public function convertPdfFirstPageToPng($pdfPath, $pngPath) {
        return $this->convertPdfToPng($pdfPath, $pngPath, true);
    }

    public function convertPdfToPng($pdfPath, $pngPath, $firstPageOnly = false) {
        if (!isset($pdfPath))
            throw new \InvalidArgumentException('pdf path not set');
        if (!isset($pngPath))
            throw new \InvalidArgumentException('png path not set');

        if (!$firstPageOnly) {
            $pngPath = str_replace('.png', '%04d.png', $pngPath);
        }

        $gsPath = $this->getGsPath();
        if (!file_exists($gsPath))
            throw new \InvalidArgumentException('ghostscript executable not found');
        $cmdConvert = sprintf(
            '%s -o "%s" -sDEVICE=pnggray -r300 %s "%s"',
            $gsPath,
            $pngPath,
            $firstPageOnly ? '-dFirstPage=1 -dLastPage=1' : '',
            $pdfPath
        );
        $this->logger->debug(sprintf(
            "converting pdf to png: %s", $cmdConvert
        ));
        $out = array();
        $res = null;
        exec($cmdConvert . ' 2>&1', $out, $res);
        $this->logger->debug(sprintf(
            "gs exit code: %s, output: %s", $res, json_encode($out)
        ));
        return $res;
    }

}
